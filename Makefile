BINARIES = adprefmed prefmed prefmedslow seqmed uintgen auintgen aduintgen
CFLAGS = -Wall -O3 -DNDEBUG

.PHONY: all clean

all: $(BINARIES)

clean:
	rm -f $(BINARIES) *.o


adprefmed: medmultiset.o multiset.o seqread.o seqwrite.o multiset.o

adprefmed.o: medmultiset.h seqread.h seqwrite.h

prefmed: heap.o medheap.o seqread.o seqwrite.o

prefmed.o: medheap.h seqread.h seqwrite.h

prefmedslow: seqread.o seqwrite.o vector.o

prefmedslow.o: seqread.h seqwrite.h vector.h

seqmed: seqread.o seqwrite.o vector.o

seqmed.o: seqread.h seqwrite.h vector.h

testmultiset: multiset.o

testmultiset.o: multiset.h


medheap.o: heap.h medheap.h

multiset.o: multiset.h

seqread.o: seqread.h

seqwrite.o: seqwrite.h

vector.o: vector.h

