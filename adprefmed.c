#include <assert.h>
#include <inttypes.h>
#include <stdio.h>

#include "medmultiset.h"
#include "seqread.h"
#include "seqwrite.h"


int
main(int argc, char **argv)
{
    int cont = 0;
    medmultiset_t *mms = medmultiset_new();
    assert(mms);
    uint64_t n;
    int action;
    while ((action = seqread_add_del_uint64(&n)) != EOF) {
	if (action == SEQREAD_ADD)
	    medmultiset_add(mms, n);
	else  /* action == SEQREAD_DEL */
	    medmultiset_del(mms, n);

	uint64_t med_lo, med_hi;
	if (medmultiset_find(mms, &med_lo, &med_hi))
	    seqwrite_uint64_mean(med_lo, med_hi, cont);
	else
	    seqwrite_na(cont);

	cont = 1;
    }

    return 0;
}
