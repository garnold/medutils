#include <assert.h>
#include <inttypes.h>
#include <stdio.h>

#include "heap.h"


/* The initial number of values that can be stored in a fresh heap. Will grow
 * (by means of realloc) by a factor of two whenever we are out of space.  */
#define HEAP_INIT_SIZE (1 << 10)

/* Returns the index of the parent of the node represented by the given index;
 * both input and output indices are zero-based. */
#define PARENT(n) ((((n) + 1) >> 1) - 1)
#define LCHILD(n) ((((n) + 1) << 1) - 1)
#define RCHILD(n) (((n) + 1) << 1)


heap_t *
heap_new(int is_max)
{
    heap_t *h = (heap_t *)malloc(sizeof(heap_t));
    h->count = 0;
    h->size = HEAP_INIT_SIZE;
    h->buf = (uint64_t *)malloc(sizeof(uint64_t) * h->size);
    h->is_max = is_max;
    return h;
}

void
heap_print_recur(uint64_t *buf, unsigned depth, size_t root, size_t count)
{
    if (root >= count)
	return;

    printf("%*s%" PRIu64 " (%zu/%u)\n", depth, "", buf[root], root, depth);
    heap_print_recur(buf, depth + 1, LCHILD(root), count);
    heap_print_recur(buf, depth + 1, RCHILD(root), count);
}

void
heap_print(heap_t *h)
{
    assert(h);
    heap_print_recur(h->buf, 0, 0, h->count);
}

void
heap_insert(heap_t *h, uint64_t n)
{
    assert(h);

    /* Grow the heap buffer if we're out of space. */
    if (h->count == h->size) {
	h->size <<= 1;
	h->buf = (uint64_t *)realloc(h->buf, sizeof(uint64_t) * h->size);
	assert(h->buf);
    }
    uint64_t *buf = h->buf;

    /* Allocate a new node. */
    size_t curr = h->count++;

    /* Bubble new value up towards the root. */
    while (curr) {
	const size_t parent = PARENT(curr);
	uint64_t parent_n = buf[parent];
	if (h->is_max ? parent_n >= n : parent_n <= n)
	    break;  /* No need to bubble up further. */
	buf[curr] = parent_n;
	curr = parent;
    }

    buf[curr] = n;
}

#ifndef NDEBUG
uint64_t
heap_root_find(heap_t *h)
{
    assert(h && h->count);
    return h->buf[0];
}
#endif /* NDEBUG */

static uint64_t
gravity(heap_t *h, size_t son, uint64_t n)
{
    assert(h);

    if (son >= h->count)
	return 0;

    uint64_t son_n = h->buf[son];
    if (h->is_max)
	return (son_n > n ? son_n - n : 0);
    else
	return (son_n < n ? n - son_n : 0);
}

uint64_t
heap_root_replace(heap_t *h, uint64_t n)
{
    assert(h);

    /* Replace the value at the root. */
    uint64_t old_root = heap_root_find(h);
    h->buf[0] = n;

    /* Bubble new root value down towards the leaves. */
    size_t curr = 0;
    while (1) {
	/* Should we bubble it to the left or right? */
	const size_t lchild = LCHILD(curr);
	const size_t rchild = RCHILD(curr);
	const uint64_t lgravity = gravity(h, lchild, n);
	const uint64_t rgravity = gravity(h, rchild, n);
	if (!(lgravity || rgravity))
	    break;
	const size_t child = (lgravity - rgravity <= lgravity ?  lchild : rchild);

	/* Swap values with chosen child. */
	uint64_t tmp = h->buf[child];
	h->buf[child] = n;
	h->buf[curr] = tmp;
	curr = child;
    }

    return old_root;
}
