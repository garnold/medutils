#ifndef __HEAP_H
#define __HEAP_H

#include <inttypes.h>
#include <stdlib.h>


typedef struct {
    size_t count;
    size_t size;
    int is_max;
    uint64_t *buf;
} heap_t;


heap_t *heap_new(int);
void heap_print(heap_t *);
void heap_insert(heap_t *, uint64_t);
#ifdef NDEBUG
# define heap_root_find(h) (*((h)->buf))
#else /* NDEBUG */
uint64_t heap_root_find(heap_t *);
#endif /* NDEBUG */
uint64_t heap_root_replace(heap_t *, uint64_t);

#endif /* __HEAP_H */
