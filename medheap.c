#include <assert.h>
#include <inttypes.h>
#include <stdio.h>

#include "heap.h"
#include "medheap.h"


medheap_t *
medheap_new(void)
{
    medheap_t *mh = (medheap_t *)malloc(sizeof(medheap_t));
    mh->lo_heap = heap_new(1);
    mh->hi_heap = heap_new(0);
    return mh;
}

void
medheap_insert(medheap_t *mh, uint64_t n)
{
    if (mh->lo_heap->count && n <= heap_root_find(mh->lo_heap)) {
	/* Value belongs in the lower half. */
	if (mh->lo_heap->count > mh->hi_heap->count) {
	    /* Replace old max root with new value. */
	    uint64_t old_max_root = heap_root_replace(mh->lo_heap, n);

	    /* Insert old root value into other heap. */
	    heap_insert(mh->hi_heap, old_max_root);
	} else {
	    /* Insert new value. */
	    heap_insert(mh->lo_heap, n);
	}
    } else if (mh->hi_heap->count && n >= heap_root_find(mh->hi_heap)) {
	/* Value belongs in the higher half. */
	if (mh->hi_heap->count > mh->lo_heap->count) {
	    /* Replace old min root with new value. */
	    uint64_t old_min_root = heap_root_replace(mh->hi_heap, n);

	    /* Insert old root value into other heap. */
	    heap_insert(mh->lo_heap, old_min_root);
	} else {
	    /* Insert new value. */
	    heap_insert(mh->hi_heap, n);
	}
    } else {
	/* Value could be in either, place in smaller (prefer lower half). */
	if (mh->hi_heap->count < mh->lo_heap->count)
	    heap_insert(mh->hi_heap, n);
	else
	    heap_insert(mh->lo_heap, n);
    }
}

int
medheap_find(medheap_t *mh, uint64_t *out_lo_p, uint64_t *out_hi_p)
{
    size_t lo_count = mh->lo_heap->count;
    size_t hi_count = mh->hi_heap->count;

    if (!(lo_count + hi_count))
	return 0;  /* It's empty. */

    *out_lo_p = heap_root_find(hi_count > lo_count ?
			       mh->hi_heap : mh->lo_heap);
    *out_hi_p = heap_root_find(lo_count > hi_count ?
			       mh->lo_heap : mh->hi_heap);
    return 1;  /* Values written. */
}
