#ifndef __MEDHEAP_H
#define __MEDHEAP_H

#include <inttypes.h>

#include "heap.h"


typedef struct {
    heap_t *lo_heap;
    heap_t *hi_heap;
} medheap_t;


medheap_t *medheap_new(void);
void medheap_insert(medheap_t *, uint64_t);
int medheap_find(medheap_t *, uint64_t *, uint64_t *);

#endif /* __MEDHEAP_H */
