#include <assert.h>
#include <inttypes.h>
#include <stdio.h>

#include "multiset.h"
#include "medmultiset.h"


medmultiset_t *
medmultiset_new(void)
{
    medmultiset_t *mms = (medmultiset_t *)malloc(sizeof(medmultiset_t));
    mms->lo_multiset = multiset_new();
    mms->hi_multiset = multiset_new();
    return mms;
}

void
medmultiset_add(medmultiset_t *mms, uint64_t n)
{
    if (mms->lo_multiset->count && n <= multiset_max(mms->lo_multiset)) {
	/* Value belongs in lower half, but might need to migrate the max. */
	if (mms->lo_multiset->count > mms->hi_multiset->count)
	    multiset_add(mms->hi_multiset, multiset_max_del(mms->lo_multiset));
	multiset_add(mms->lo_multiset, n);
    } else if (mms->hi_multiset->count && n >= multiset_min(mms->hi_multiset)) {
	/* Value belongs in higher half, but might need to migrate the min. */
	if (mms->hi_multiset->count > mms->lo_multiset->count)
	    multiset_add(mms->lo_multiset, multiset_min_del(mms->hi_multiset));
	multiset_add(mms->hi_multiset, n);
    } else {
	/* Value could be in either, place in smaller (prefer lower half). */
	if (mms->hi_multiset->count < mms->lo_multiset->count)
	    multiset_add(mms->hi_multiset, n);
	else
	    multiset_add(mms->lo_multiset, n);
    }
}

void
medmultiset_del(medmultiset_t *mms, uint64_t n)
{
    multiset_t *lo_multiset = mms->lo_multiset;
    multiset_t *hi_multiset = mms->hi_multiset;
    size_t lo_count = lo_multiset->count;
    size_t hi_count = hi_multiset->count;
    uint64_t lo_max = (lo_count ? multiset_max(lo_multiset) : 0);
    uint64_t hi_min = (hi_count ? multiset_min(hi_multiset) : 0);

    if ((!lo_count || n > lo_max) && (!hi_count || n < hi_min))
	return;  /* Value isn't in any of the multisets. */

    if (!hi_count || n < hi_min) {
	/* Definitely not in upper half, remove from lower half. */
	multiset_del(lo_multiset, n);
    } else if (!lo_count || n > lo_max ||
	     hi_multiset->count >= lo_multiset->count) {
	/* Definitely not in lower, or it's in either and higher is bigger, so
	 * remove from it. */
	multiset_del(hi_multiset, n);
    } else {
	/* It's in either and lower is bigger, remove from it. */
	multiset_del(lo_multiset, n);
    }

    /* Fix load imbalance. */
    if (lo_multiset->count > hi_multiset->count + 1)
	multiset_add(hi_multiset, multiset_max_del(lo_multiset));
    else if (hi_multiset->count > lo_multiset->count + 1)
	multiset_add(lo_multiset, multiset_min_del(hi_multiset));
}

int
medmultiset_find(medmultiset_t *mms, uint64_t *out_lo_p, uint64_t *out_hi_p)
{
    size_t lo_count = mms->lo_multiset->count;
    size_t hi_count = mms->hi_multiset->count;

    if (!(lo_count + hi_count))
	return 0;  /* It's empty. */

    *out_lo_p = (hi_count > lo_count ? multiset_min(mms->hi_multiset) :
		 multiset_max(mms->lo_multiset));
    *out_hi_p = (lo_count > hi_count ? multiset_max(mms->lo_multiset) :
		 multiset_min(mms->hi_multiset));
    return 1;  /* Values written. */
}
