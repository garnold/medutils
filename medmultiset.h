#ifndef __MEDMULTISET_H
#define __MEDMULTISET_H

#include <inttypes.h>

#include "multiset.h"


typedef struct {
    multiset_t *lo_multiset;
    multiset_t *hi_multiset;
} medmultiset_t;


medmultiset_t *medmultiset_new(void);
void medmultiset_add(medmultiset_t *, uint64_t);
void medmultiset_del(medmultiset_t *, uint64_t);
int medmultiset_find(medmultiset_t *, uint64_t *, uint64_t *);

#endif /* __MEDMULTISET_H */

