#include <assert.h>
#include <stdio.h>

#include "multiset.h"


#define MULTISET_BUF_INIT_SIZE (1 << 10)
#define KEY_LEN_MAX (sizeof(uint64_t) * 8)
#define KEY_MASK(n, l) (((n) >> (64 - (l))) << (64 - (l)))
#define KEY_MATCH(k, n, l) ((k) == KEY_MASK(n, l))
#define KEY_NEXT(k, l) ((int)(((k) >> (63 - (l))) & (uint64_t)0x1))


multiset_t *
multiset_new(void)
{
    multiset_t *ms = (multiset_t *)malloc(sizeof(multiset_t));
    ms->count = 0;
    ms->num_nodes = 0;

    ms->size = MULTISET_BUF_INIT_SIZE;
    ms->buf = (node_t *)malloc(ms->size * sizeof(node_t));
    ms->fresh_head = 0;
    ms->recycled_head = SIZE_MAX;

    ms->root = SIZE_MAX;
    ms->min_cache = SIZE_MAX;
    ms->max_cache = SIZE_MAX;

    return ms;
}

static void
multiset_print_recur(multiset_t *ms, size_t root, int depth)
{
    if (root == SIZE_MAX)
	return;
    node_t *msn = ms->buf + root;
    printf("%*s%zu: 0x%016" PRIx64 "/%u (%" PRIu64 ") ",
	   depth, "", root, msn->u.live.key, msn->u.live.key_len,
	   msn->u.live.key);
    int internal = msn->u.live.key_len < KEY_LEN_MAX;
    if (internal)
	printf("l=%zu r=%zu", msn->u.live.u.int_children[0],
	       msn->u.live.u.int_children[1]);
    else
	printf("[%zu]", msn->u.live.u.leaf_count);
    printf("\n");

    if (internal) {
	multiset_print_recur(ms, msn->u.live.u.int_children[0], depth + 1);
	multiset_print_recur(ms, msn->u.live.u.int_children[1], depth + 1);
    }
}

void
multiset_print(multiset_t *ms)
{
    printf("Using %zu/%zu (%zu fresh), total count=%zu\n",
	   ms->num_nodes, ms->size, ms->size - ms->fresh_head,
	   ms->count);
    multiset_print_recur(ms, ms->root, 0);
}

static size_t
multiset_node_alloc(multiset_t *ms)
{
    size_t new = ms->recycled_head;
    if (new < SIZE_MAX) {
	/* We can consume a recycled node. */
	ms->recycled_head = ms->buf[new].u.recycled_next;
    } else {
	/* We need to consume a fresh node. */
	if (ms->fresh_head == ms->size) {
	    /* No space, double the pool's size. */
	    ms->size <<= 1;
	    ms->buf = (node_t *)realloc(ms->buf, sizeof(node_t) * ms->size);
	    assert(ms->buf);
	}
	new = ms->fresh_head++;
    }

    /* Record node usage. */
    ms->num_nodes++;

    return new;
}

static void
multiset_node_recycle(multiset_t *ms, size_t node)
{
    ms->buf[node].u.recycled_next = ms->recycled_head;
    ms->recycled_head = node;
    ms->num_nodes--;
}

void
multiset_add(multiset_t *ms, uint64_t n)
{
    /* Find preexisting, or farthest tying point. */
    size_t parent = SIZE_MAX;
    size_t next = ms->root;
    int child;
    int leftmost_path = 1;
    int rightmost_path = 1;
    if (next < SIZE_MAX) {
	while (1) {
	    node_t *next_msn = ms->buf + next;
	    unsigned key_len = next_msn->u.live.key_len;

	    if (!KEY_MATCH(next_msn->u.live.key, n, key_len))
		break;

	    if (key_len == KEY_LEN_MAX) {
		ms->count++;
		next_msn->u.live.u.leaf_count++;
		return;
	    }

	    parent = next;
	    child = KEY_NEXT(n, key_len);
	    if (child)
		leftmost_path = 0;
	    else
		rightmost_path = 0;
	    next = next_msn->u.live.u.int_children[child];
	    assert(next < SIZE_MAX);  /* This is a structural invariant. */
	}
    }

    /* Allocate new leaf. */
    size_t leaf = multiset_node_alloc(ms);
    node_t *leaf_msn = ms->buf + leaf;
    leaf_msn->u.live.key = n;
    leaf_msn->u.live.key_len = KEY_LEN_MAX;
    leaf_msn->u.live.u.leaf_count = 1;

    /* Decide whether we need an new internal node. */
    size_t tie = leaf;
    if (next < SIZE_MAX) {
	node_t *next_msn = ms->buf + next;

	/* Find the longest common  prefix. */
	int internal_key_len = __builtin_clzll(n ^ next_msn->u.live.key);

	/* Allocate an internal node. */
	size_t internal = multiset_node_alloc(ms);
	node_t *internal_msn = ms->buf + internal;
	internal_msn->u.live.key = KEY_MASK(n, internal_key_len);;
	internal_msn->u.live.key_len = internal_key_len;

	/* Tie internal node's children correctly. */
	int leaf_child = KEY_NEXT(n, internal_key_len);
	internal_msn->u.live.u.int_children[leaf_child] = leaf;
	internal_msn->u.live.u.int_children[!leaf_child] = next;

	/* Be sure to invalidate min/max caches, as needed. */
	if (leaf_child) {
	    if (rightmost_path)
		ms->max_cache = SIZE_MAX;
	} else {
	    if (leftmost_path)
		ms->min_cache = SIZE_MAX;
	}

	tie = internal;
    };

    /* Tie the new node(s) to the right place. */
    if (parent == SIZE_MAX)
	ms->root = tie;  /* As root. */
    else
	ms->buf[parent].u.live.u.int_children[child] = tie;  /* To internal node. */

    ms->count++;
}

static size_t
multiset_del_helper(multiset_t *ms, uint64_t n, int use_n, int seek_max)
{
    /* Look for match, recording the farthest ancestors. */
    size_t parent = SIZE_MAX;
    size_t grandparent = SIZE_MAX;
    size_t leaf = ms->root;
    if (leaf == SIZE_MAX)
	return 0;  /* Empty multiset, arbitrary return value. */
    int child, grandchild;
    node_t *leaf_msn;
    while (1) {
	leaf_msn = ms->buf + leaf;
	unsigned key_len = leaf_msn->u.live.key_len;

	if (use_n && !KEY_MATCH(leaf_msn->u.live.key, n, key_len))
	    return 0;  /* Not in multiset, arbitrary return value. */

	if (key_len == KEY_LEN_MAX)
	    break;  /* Found it. */

	/* Where do we go next? */
	grandchild = child;
	if (use_n)
	    child = KEY_NEXT(n, key_len);
	else
	    child = seek_max;

	grandparent = parent;
	parent = leaf;
	leaf = leaf_msn->u.live.u.int_children[child];
	assert(leaf < SIZE_MAX);  /* This is a structural invariant. */
    }

    ms->count--;
    n = leaf_msn->u.live.key;

    /* If the count remains positive, we're done. */
    if (--leaf_msn->u.live.u.leaf_count)
	return n;  /* Still here, count decremented. */

    /* Leaf has to go, make sure to invalidate min/max caches. */
    if (leaf == ms->min_cache)
	ms->min_cache = SIZE_MAX;
    if (leaf == ms->max_cache)
	ms->max_cache = SIZE_MAX;

    if (parent == SIZE_MAX)
	ms->root = SIZE_MAX;  /* This is the only node in the tree. */
    else {
	/* Parent goes too, we tie sibling directly to grandparent. */
	size_t sibling = ms->buf[parent].u.live.u.int_children[!child];

	if (grandparent == SIZE_MAX) {
	    /* No grandparent, tie sibling as root. */
	    ms->root = sibling;
	} else {
	    /* Tie sibling to grandparent. */
	    ms->buf[grandparent].u.live.u.int_children[grandchild] = sibling;
	}

	/* Recycle the parent. */
	multiset_node_recycle(ms, parent);
    }

    /* Recycle the leaf node. */
    multiset_node_recycle(ms, leaf);

    return n;  /* Removed from multiset. */
}

void
multiset_del(multiset_t *ms, uint64_t n)
{
     multiset_del_helper(ms, n, 1, 0);
}

static size_t
multiset_recache(multiset_t *ms, size_t *cache_p, const int path)
{
    if (*cache_p < SIZE_MAX)
	return *cache_p;

    size_t next = ms->root;
    assert(next < SIZE_MAX);  /* Don't use on an empty heap. */
    while (ms->buf[next].u.live.key_len < KEY_LEN_MAX) {
	next = ms->buf[next].u.live.u.int_children[path];
	assert(next < SIZE_MAX);  /* This is a structural invariant. */
    }
    return (*cache_p = next);
}

uint64_t
multiset_min(multiset_t *ms)
{
    return ms->buf[multiset_recache(ms, &ms->min_cache, 0)].u.live.key;
}


uint64_t
multiset_max(multiset_t *ms)
{
    return ms->buf[multiset_recache(ms, &ms->max_cache, 1)].u.live.key;
}

uint64_t
multiset_min_del(multiset_t *ms)
{
    return multiset_del_helper(ms, 0, 0, 0);
}

uint64_t
multiset_max_del(multiset_t *ms)
{
    return multiset_del_helper(ms, 0, 0, 1);
}
