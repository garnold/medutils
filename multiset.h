#ifndef __MULTISET_H
#define __MULTISET_H

#include <inttypes.h>
#include <stdlib.h>


typedef struct {
    union {
        struct {
            uint64_t key;
            unsigned key_len;
            union {
                size_t leaf_count;       /* Value store in a leaf. */
                size_t int_children[2];  /* Children of an internal node. */
            } u;
        } live;
        size_t recycled_next;            /* Recycled list pointer. */
    } u;
} node_t;


typedef struct {
    size_t count;
    size_t num_nodes;

    /* Node pool and recycled node list. */
    size_t size;
    node_t *buf;
    size_t fresh_head;
    size_t recycled_head;

    /* Search tree root. */
    size_t root;
    size_t min_cache;
    size_t max_cache;
} multiset_t;


multiset_t *multiset_new(void);
void multiset_print(multiset_t *);
void multiset_add(multiset_t *, uint64_t);
void multiset_del(multiset_t *, uint64_t);
uint64_t multiset_min(multiset_t *);
uint64_t multiset_max(multiset_t *);
uint64_t multiset_min_del(multiset_t *);
uint64_t multiset_max_del(multiset_t *);

#endif /* __MULTISET_H */
