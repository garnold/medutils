#include <assert.h>
#include <inttypes.h>
#include <stdio.h>

#include "medheap.h"
#include "seqread.h"
#include "seqwrite.h"


int
main(int argc, char **argv)
{
    int cont = 0;
    medheap_t *mh = medheap_new();
    assert(mh);
    uint64_t n;
    while (seqread_uint64(&n) != EOF) {
	medheap_insert(mh, n);

	uint64_t med_lo, med_hi;
	if (medheap_find(mh, &med_lo, &med_hi))
	    seqwrite_uint64_mean(med_lo, med_hi, cont);
	else
	    seqwrite_na(cont);

	cont = 1;
    }

    return 0;
}
