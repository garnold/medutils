#include <assert.h>
#include <inttypes.h>
#include <stdio.h>

#include "seqread.h"
#include "seqwrite.h"
#include "vector.h"


int
main(int argc, char **argv)
{
    int cont = 0;
    vector_t *v = vector_new();
    assert(v);
    uint64_t n;
    while (seqread_uint64(&n) != EOF) {
	vector_append(v, n);
	vector_sort(v);
	seqwrite_uint64_mean(vector_get(v, (v->count - 1) / 2),
			     vector_get(v, v->count / 2),
			     cont);
	cont = 1;
    }
    return 0;
}
