#include <assert.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

#include "seqread.h"
#include "seqwrite.h"
#include "vector.h"


int
main(int argc, char **argv)
{
    vector_t *v = vector_new();
    assert(v);
    uint64_t n;
    while (seqread_uint64(&n) != EOF)
	vector_append(v, n);
    if (!v->count)
	return 0;

    vector_sort(v);

    seqwrite_uint64_mean(vector_get(v, (v->count - 1) / 2),
			 vector_get(v, v->count / 2), 0);
    return 0;
}
