#include <assert.h>
#include <inttypes.h>
#include <stdio.h>

#include "seqread.h"


int
seqread_uint64(uint64_t *out_p)
{
    assert(out_p);
    uint64_t out = 0;
    int c;
    while ((c = getchar()) != ' ') {
	if (c == EOF)
	    return EOF;
	out *= 10;
	out += c - '0';
    }
    *out_p = out;
    return 0;
}

int
seqread_add_del_uint64(uint64_t *out_p)
{
    int c = getchar();
    int ret = (c == '+' ? SEQREAD_ADD : SEQREAD_DEL);
    return (seqread_uint64(out_p) == EOF ? EOF : ret);
}
