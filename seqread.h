#ifndef __SEQREAD_H
#define __SEQREAD_H

#include <inttypes.h>


enum {
    SEQREAD_ADD,
    SEQREAD_DEL,
};


/* Reads a 64-bit unsigned integer from stdin, until a space or EOF is
 * encountered. If no value was read, returns EOF and nothing is written to
 * |ret_p|. Otherwise, writes the value to |ret_p| and returns zero.  This
 * function does *not* check that the input is valid. Note that the space
 * delimiter is being consumed from stdin. */
int seqread_uint64(uint64_t *);

/* Reads either '+' or '-' followed by a 64-bit unsigned integer from stdin,
 * until a space or EOF is encountered. If no value was read, returns EOF and
 * nothing is written to |ret_p|. Otherwise, writes the value to |ret_p| and
 * returns SEQREAD_ADD or SEQREAD_DEL respectively, depending on the prefix
 * symbol.  This function does *not* check that the input is valid. Note that
 * the space delimiter is being consumed from stdin. */
int seqread_add_del_uint64(uint64_t *);

#endif /* __SEQREAD_H */
