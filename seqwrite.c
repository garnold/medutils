#include <inttypes.h>
#include <stdio.h>

#include "seqwrite.h"


int
seqwrite_uint64_mean(uint64_t a, uint64_t b, int leading_space)
{
    int rem = (a & 0x1) + (b & 0x1);
    a >>= 1;
    b >>= 1;
    uint64_t mean = a + b + (rem ==2);
    return printf("%s%" PRIu64 "%s", (leading_space ? " " : ""), mean,
		  (rem == 1 ?  ".5" : ""));
}

int
seqwrite_na(int leading_space)
{
    return printf("%sNA", (leading_space ? " " : ""));
}
