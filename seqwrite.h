#ifndef __SEQWRITE_H
#define __SEQWRITE_H

#include <inttypes.h>


/* Outputs the mean of two 64-bit integers to stdout, optionally with a leading
 * space. Note that, by definition, this is either an integer (if the sum is
 * even) or xyz.5 otherwise. Also note that the function will *not* overflow
 * even if the sum of the two integer exceeds the capacity of uint64_t. Returns
 * the number of bytes written. */
int seqwrite_uint64_mean(uint64_t, uint64_t, int);

/* This writes the string 'NA' to stdout, possibly with a leading space. Returns
 * the number of characters written. */
int seqwrite_na(int);

#endif /* __SEQWRITE_H */
