#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

int
main(int argc, char **argv)
{
    assert(argc == 2);

    long unsigned count;
    int cont = 0;
    for (count = strtoul(argv[1], NULL, 10); count; count--) {
	long r = random();
	printf((cont ? " %ld" : "%ld"), r);
	cont = 1;
    }

    return 0;
}
