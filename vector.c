#include <assert.h>
#include <inttypes.h>
#include <stdlib.h>

#include "vector.h"


#define VEC_INIT_SIZE (1 << 10)


vector_t *
vector_new(void)
{
    vector_t *v = (vector_t *)malloc(sizeof(vector_t));
    assert(v);
    v->size = VEC_INIT_SIZE;
    v->count = 0;
    v->buf = (uint64_t *)malloc(sizeof(uint64_t) * v->size);
    return v;
}

void
vector_append(vector_t *v, uint64_t n)
{
    if (v->count == v->size) {
	v->size <<= 1;
	v->buf = (uint64_t *)realloc(v->buf, sizeof(uint64_t) * v->size);
	assert(v->buf);
    }
    v->buf[v->count++] = n;
}

static int
uint64_cmp(const void *a_p, const void *b_p)
{
    uint64_t a = *((uint64_t *)a_p), b = *((uint64_t *)b_p);
    return (a < b ? -1 : a > b);
}

void
vector_sort(vector_t *v)
{
    qsort(v->buf, v->count, sizeof(uint64_t), uint64_cmp);
}

uint64_t
vector_get(vector_t *v, size_t i)
{
    return v->buf[i];
}

size_t
vector_count(vector_t *v)
{
    return v->count;
}
