#ifndef __VECTOR_H
#define __VECTOR_H

#include <inttypes.h>


typedef struct {
    size_t count;
    size_t size;
    uint64_t *buf;
} vector_t;


vector_t *vector_new(void);
void vector_append(vector_t *, uint64_t);
void vector_sort(vector_t *);
uint64_t vector_get(vector_t *, size_t);
size_t vector_count(vector_t *);

#endif /* __VECTOR_H */
